<?php

use App\Http\Controllers\Auth\AuthorisationController;
use App\Http\Controllers\Auth\PasswordResetController;
use App\Http\Controllers\Auth\RegistrationController;
use App\Http\Controllers\Company\Invoices\InvoiceController;
use App\Http\Controllers\Company\Media\AvatarController;
use App\Http\Controllers\Company\Media\CoverController;
use App\Http\Controllers\Company\PaymentMethods\PaymentMethodController;
use App\Http\Controllers\Company\Statistics\StatisticController;
use App\Http\Controllers\Company\Vacancies\Candidtas\CandidateController;
use App\Http\Controllers\Guest\Companies\Company\Vacancies\VacancyController;
use App\Http\Controllers\Guest\Companies\FilterController;
use App\Http\Controllers\Guest\VacancyCategories;
use App\Http\Controllers\Guest\Vacancies\VacanciesController;
use App\Http\Controllers\Guest\Vacancies\VacancyFilterController;
use App\Http\Controllers\Guest\Companies\CompanyController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ResumeController;
use App\Http\Controllers\User\Companies\FollowedCompaniesController;
use App\Http\Controllers\User\Companies\CompanyController as NonUserCompanyController;
use App\Http\Controllers\User\Cv\CvController;
use App\Http\Controllers\User\Profile\ProfileController;
use App\Http\Controllers\User\Profile\Validate\EmailValidationController;
use App\Http\Controllers\User\Profile\Validate\PhoneValidationController;
use App\Http\Controllers\User\Vacancies\AppliedVacanciesController;
use App\Http\Controllers\User\Vacancies\SavedVacanciesController;
use App\Http\Controllers\User\Vacancies\VacancyController as UserVacancyController;
use App\Http\Controllers\Auth\VerificationController;
use Illuminate\Support\Facades\Route;
Auth::routes(['verify' => true]);



Route::get('', [IndexController::class, 'index']);
Route::get('/vacancies', [VacanciesController::class, 'list']);
Route::get('/vacancies/{vacancyId}', [VacanciesController::class, 'view']);
Route::post('/vacancies', [VacanciesController::class, 'search']);



Route::get('/companies', [CompanyController::class, 'list']);
Route::get('/companies/{companyId}', [CompanyController::class, 'view']);
Route::post('/companies', [CompanyController::class, 'search']);

Route::post('/auth/register', [RegistrationController::class, 'register']);
Route::post('/auth/login', [AuthorisationController::class, 'login']);
Route::post('/auth/password/email', [PasswordResetController::class,'forgot']);
Route::post('/auth/password/reset', [PasswordResetController::class,'reset']);

Route::group(['middleware' => ['auth:sanctum','verified'], 'prefix' => 'companies'], function () {
    Route::get('{company}/follow', [NonUserCompanyController::class, 'follow']);
    Route::delete('{company}/unfollow', [NonUserCompanyController::class, 'unFollow']);
});
Route::get('/me/verify/email/resend',[ProfileController::class,'resendEmailVerificationLink'])->middleware(['auth:sanctum']);
Route::group(['middleware' => ['auth:sanctum','verified'], 'prefix' => 'vacancies'], function () {

    Route::get('{vacancy}/save', [SavedVacanciesController::class, 'save']);
    Route::delete('{vacancy}/remove', [SavedVacanciesController::class, 'remove']);
    Route::get('{vacancy}/{resume}/apply', [UserVacancyController::class, 'apply']);
});
Route::group(['middleware' => ['auth:sanctum','verified'], 'prefix' => '/me'], function () {
    Route::get('', [ProfileController::class, 'getMyInfo']);
    Route::put('/password', [ProfileController::class, 'changePassword']);
    Route::put('', [ProfileController::class, 'update']);
    Route::delete('', [ProfileController::class, 'logout']);
    Route::post('/avatar', [ProfileController::class, 'upload']);
    Route::delete('/avatar', [ProfileController::class, 'removeAvatar']);
    Route::get('/followed/companies', [FollowedCompaniesController::class, 'list']);
    Route::get('/saved/vacancies', [SavedVacanciesController::class, 'list']);
    Route::get('/applied/vacancies', [AppliedVacanciesController::class, 'list']);



    Route::group(['prefix' => '/cv'], function () {
        Route::get('', [CvController::class, 'list']);
        Route::post('', [CvController::class, 'store']);
        Route::get('{resume}', [CvController::class, 'show']);
        Route::get('{resume}/makedefault', [CvController::class, 'makeDefault']);
        Route::put('{resume}', [CvController::class, 'update']);
        Route::delete('{resume}', [CvController::class, 'destroy']);
    });

    Route::group(['prefix' => '/validate'], function () {

        Route::get('phone', [PhoneValidationController::class, 'sendValidationCode']);
        Route::get('phone/{code}', [PhoneValidationController::class, 'validateCode']);
    });




});
Route::get('/specialisations', [VacancyCategories::class, 'list']);
Route::get('/contracttypes', [\App\Http\Controllers\ContractTypesController::class, 'list']);
Route::post('/search', [VacancyFilterController::class, 'search']);

Route::get('/candidate/{candidate}/download/{hash}', [ResumeController::class, 'download'])->name('downloadResume');

