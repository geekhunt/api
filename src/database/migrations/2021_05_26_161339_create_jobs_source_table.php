<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsSourceTable extends Migration
{
    public function up()
    {
        Schema::create('jobs_source', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('source')->index();
            $table->string('source_id');
            $table->string('title');
            $table->string('company');
            $table->foreignId('company_id')->nullable()->constrained('companies');
            $table->date('start_date');
            $table->date('end_date');
            $table->dateTime('log_date');
            $table->enum('status',['not_in_our_system','pending','in_our_system']);


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jobs_source');
    }
}
