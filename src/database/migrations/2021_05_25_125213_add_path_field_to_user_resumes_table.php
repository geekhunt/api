<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPathFieldToUserResumesTable extends Migration
{
    public function up()
    {
        Schema::table('user_resumes', function (Blueprint $table) {
            $table->string('path')->after('name');
            $table->string('hash')->after('is_default');
        });
    }

    public function down()
    {
        Schema::table('user_resumes', function (Blueprint $table) {
            $table->dropColumn('path');
            $table->dropColumn('hash');
        });
    }
}
