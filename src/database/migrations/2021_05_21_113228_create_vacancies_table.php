<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->text('description');
            $table->foreignId('company_id')->constrained('companies')->references('id');
            $table->foreignId('specialisation_id')->constrained('specialisations')->references('id');
            $table->date('end_date')->nullable();
            $table->date('start_date')->nullable();
            $table->integer('views')->default(0);
            $table->tinyInteger('is_freelance')->default(0);
            $table->tinyInteger('is_remote')->default(0);
            $table->char('work_start_time',8);
            $table->char('work_end_time',8);
            $table->decimal('salary_start',8,2,true)->nullable();
            $table->decimal('salary_end',8,2,true)->nullable();
            $table->integer('salary_currency')->default(981)->nullable();
            $table->foreignId('salary_type_id')->constrained('salary_types')->references('id');
            $table->foreignId('contract_type_id')->default(1)->constrained('contract_types')->references('id');
            $table->boolean('is_draft')->default(true);
            $table->boolean('is_active')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
