<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->foreignId('admin_id')->nullable()->constrained('users');
            $table->string('avatar',255)->nullable();
            $table->string('cover',255)->nullable();
            $table->year('founded')->nullable();
            $table->string('email')->nullable();
            $table->string('legal_name')->nullable();
            $table->string('legal_number')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('slogan')->nullable();
            $table->text('about')->nullable();
            $table->boolean('is_active')->default(false);
            $table->string('field_area')->nullable();
            $table->char('background_color',7)->default("#ffffff")->nullable();
            $table->char('brand_color',7)->default("#ffffff")->nullable();
            $table->boolean('is_archived')->default(false);
            $table->timestamp('archived_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
