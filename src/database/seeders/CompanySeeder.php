<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $url = 'https://api.tt.ge/search/companies?state=Active&take=100';
        $response = Http::get($url);
        $data = $response->json();

        foreach ($data['items'] as $item) {
            $user = User::factory()->create();

            $params = [
                'name' => $item['name'],
                'slug' => Str::slug($item['name']),
                'email' => $user->email,
                'legal_name' => $item['name'],
                'slogan' => $item['primaryText'],
                'about' => $item['about'],
                'is_active' => 1,
                'field_area' => $item['secondaryText'],
                'background_color' => $item['backgroundColor'],
                'text_color' => $item['primaryTextColor'],
                'secondary_color' => $item['secondaryTextColor'],

            ];
            if (is_numeric($item['founded'])) {
                $params['founded'] = $item['founded'];
            }
            $company = Company::create($params);

            $company->addMediaFromUrl('https://s3.eu-central-1.amazonaws.com/public.tt.ge' . $item['logoUrl'])
                ->toMediaCollection('avatar');
            CompanyUser::create(
                ['user_id' => $user->id, 'company_id' => $company->id, 'is_admin' => 1]
            );
        }
    }
}
