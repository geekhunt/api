<?php

namespace Database\Seeders;

use App\Models\ContractType;
use Illuminate\Database\Seeder;

class ContractTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContractType::insert([
            'name'=>'Full-time',
            'name'=>'Part-time',
            'name'=>'Contract',
        ]);
    }
}
