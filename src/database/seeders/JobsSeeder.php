<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Vacancy;
use App\Models\Specialisation;
use App\Models\Skill;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class JobsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $url = 'https://api.tt.ge/search/jobs?take=100&withCount=true&state=Active';
        $response = \Http::get($url);
        $data = $response->json();
        foreach ($data['items'] as $item) {
            $date = substr($item['createdAt'],0,10);
            $company = Company::where('name',$item['company']['name'])->firstOrFail();
            if(key_exists('name',$item['category']))
            {
                array_push($item['tags'],$item['category']['name']);
            }
            $typeId = 1;
            if(key_exists('name',$item['field']) && !empty($item['field']['slug']))
            {
                $typeId= Specialisation::createOrSelect(['slug'=>Str::slug($item['field']['name'])],['name'=>$item['field']['name']]);
            }
            $lower = !is_null($item['salary']) && key_exists('lower',$item['salary']) ? $item['salary']['lower'] : null;
            $upper = !is_null($item['salary']) && key_exists('upper',$item['salary']) ? $item['salary']['upper'] : null;
            $params = [
                'company_id'=>$company->id,
                'title'=>$item['name'],
                'slug'=>Str::slug($item['name']),
                'description'=>$item['description'],
                'is_freelance'=>false,
                'is_remote'=>$item['location']=='Remote' ? 1 : 0,
                'salary_start'=>$lower,
                'salary_end'=>$upper,
                'salary_currency'=>981,
                'salary_type'=>1,
                'contract_type'=>1,
                'is_draft'=>0,
                'is_active'=>1,
                'start_date'=>Carbon::createFromFormat('Y-m-d',$date)->format('Y-m-d'),
                'deadline'=>Carbon::createFromFormat('Y-m-d',$date)->addDays(30)->format('Y-m-d')
            ];
            $params['type_id'] = $typeId->id;
            $job = Vacancy::create($params);

            foreach ($item['tags'] as $skill)
            {
                $job->skills()->attach(Skill::createOrSelect(['name'=>$skill],['slug'=>Str::slug($skill)]));
            }
        }
    }
}
