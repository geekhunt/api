<?php

namespace Database\Seeders;

use App\Models\Specialisation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class JobTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Specialisation::insert([
            ['name'=>'Web დეველოპმენტი','slug'=>Str::slug('Web დეველოპმენტი')],
            ['name'=>'Mobile დეველოპმენტი','slug'=>Str::slug('Mobile დეველოპმენტი')],
            ['name'=>'Testing','slug'=>Str::slug('Testing')],
            ['name'=>'Networking','slug'=>Str::slug('Networking')],
            ['name'=>'Security','slug'=>Str::slug('Security')]
        ]);
    }
}
