<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::insert(['id'=>981,'currency_sign'=>'₾']);
        Currency::insert(['id'=>840,'currency_sign'=>'$']);
    }
}
