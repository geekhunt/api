<?php

namespace Database\Seeders;

use App\Models\SalaryType;
use Illuminate\Database\Seeder;

class SalaryTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SalaryType::insert([
            ['name'=>'ყოველთვიური'],
            ['name'=>'ყოველდღიური'],
            ['name'=>'ყოველკვიერეული'],
            ['name'=>'საათობრივი'],
            ['name'=>'კონტრაქტი'],
        ]);
    }
}
