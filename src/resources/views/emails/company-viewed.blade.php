@component('mail::message')
@if (! empty($greeting))
 # {{ $greeting }}
@endif

@foreach ($introLines as $line)
## {{ $line }}
@endforeach
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
    {{ $actionText }}
@endcomponent
    
@endcomponent
