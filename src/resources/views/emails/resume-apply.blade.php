@component('mail::message')
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="https://geekhunt.ge/_nuxt/img/geekhunt.4bfbf54.svg" />
        @endcomponent
    @endslot
    {{-- You can change this template using File > Settings > Editor > File and Code Templates > Code > Laravel Ideal Mail --}}
მომხმარებელმა {{$user->name}} {{$user->last_name}} გამოგიგზავნათ რეზიუმე
<a href="{{route('downloadResume',['candidate'=>$candidate->id,'hash'=>$resume->hash])}}">გადმოწერა</a>

@endcomponent
