<?php
return [
    'url'=>env('WEB_URL','https://geekhunt.ge'),
    'email_verify_url'=>env('EMAIL_VERIFY_URL','/utils/resetpass?'),
];
