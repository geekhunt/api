<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserInfoTest extends TestCase
{
//    use RefreshDatabase;
    use WithFaker;
    /**
     * @test
     */
    public function me_is_a_protected_route()
    {
        $response = $this->json('GET','/api/me');
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function me_returns_user_info()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->json('GET','/api/me')->assertStatus(200);
        $response->assertJson(['status'=>'Success']);
    }

    /**
     * @test
     */
    public function user_profile_is_updating()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->create();
        dump($user->toArray());

        $data = [
            'name'=>$this->faker->firstName('male'),
            'last_name'=>$this->faker->lastName(),
            'birth_date'=>$this->faker->date('Y-m-d'),
            'gender'=>'male',
            'title'=> $this->faker->text(100),
            'about'=>$this->faker->text(100),
            'phone'=>$this->faker->e164PhoneNumber,
        ];
        $this->actingAs($user)->json('PUT','/api/me',$data)->assertStatus(200);
        $newUser = $this->actingAs($user)->json('GET','/api/me');

        $newUser->assertJson(['data'=>$data])->isOk();

    }
}
