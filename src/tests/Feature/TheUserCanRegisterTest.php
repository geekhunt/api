<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TheUserCanRegisterTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * A basic feature test example.
     * @test
     * @return void
     */
    public function email_is_required()
    {
        $response = $this->json('POST','/api/auth/register',
            ['password'=>$this->faker->password,
            'name'=>$this->faker->name
            ]
        );
        $response->assertStatus(422)
            ->assertJsonValidationErrors(['email'=>'The email field is required.']);

    }

    /**
     * @test
     */
    public function name_is_required()
    {
        $response = $this->json('POST','/api/auth/register',
            ['password'=>$this->faker->password,
                'email'=>$this->faker->email
            ]
        );
        $response->assertStatus(422)
            ->assertJsonValidationErrors(['name'=>'The name field is required.']);
    }
    /**
     * @test
     */
    public function password_is_required()
    {
        $this->json('POST','/api/auth/register',
            ['email'=>$this->faker->email,
                'name'=>$this->faker->name
            ]
        )->assertStatus(422)
            ->assertJsonValidationErrors(['password'=>'The password field is required.']);
    }

    /**
     * @test
     */
    public function password_should_confirmed()
    {
        $this->json('POST','/api/auth/register',
            ['email'=>$this->faker->email,
                'name'=>$this->faker->name,
                'password'=>$this->faker->password
            ]
        )->assertStatus(422)->assertJsonValidationErrors(['password'=>'The password confirmation does not match.']);
    }

    /**
     * @test
     */

    public function passwords_should_be_same()
    {
        $this->json('POST','/api/auth/register',
            ['email'=>$this->faker->email,
                'name'=>$this->faker->name,
                'password'=>$this->faker->password,
                'password_confirmation'=>$this->faker->password
            ]
        )->assertStatus(422)->assertJsonValidationErrors(['password'=>'The password confirmation does not match.']);
    }
    /**
     * @test
     */

        public function user_should_register()
    {
        $password = $this->faker->password;
        $email = $this->faker->email;
        $this->json('POST','/api/auth/register',
            ['email'=>$email,
                'name'=>$this->faker->name,
                'password'=>$password,
                'password_confirmation'=>$password
            ]
        )->assertStatus(200);
        $this->assertDatabaseCount('users',1);
        $user = User::where('email',$email)->first();
        $this->assertIsObject($user);
        $this->assertSame($user->email,$email);
    }
}
