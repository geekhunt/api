<?php

namespace Tests\Feature;

use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserLoginTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * @test
     */
    function username_is_required()
    {
        $response = $this->json('POST','/api/auth/login',['password'=>$this->faker->password])->assertStatus(422)
            ->assertJsonValidationErrors(['email']);
    }
    /**
     * @test
     */
    function password_is_required()
    {
        $this->json('POST','/api/auth/login',['email'=>$this->faker->email])->assertStatus(422)
            ->assertJsonValidationErrors(['password']);
    }

    /**
     * @test
     */

    function user_invalid()
    {
        $this->json('POST','/api/auth/login',['email'=>$this->faker->unique()->safeEmail,'password'=>$this->faker->password])
            ->assertStatus(401);
    }
    /**
     * @test
     */

    function login_success()
    {
        $email = $this->faker->unique()->safeEmail;
        $password = $this->faker->password;
        User::factory()->create([
            'email'=>$email,
            'password'=>bcrypt($password),
        ]);
        $this->json('POST','/api/auth/login',['email'=>$email,'password'=>$password])
            ->assertStatus(200);
    }
}
