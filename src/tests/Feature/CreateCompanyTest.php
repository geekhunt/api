<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class CreateCompanyTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * @test
     */
    public function company_name_is_required()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->json('POST','/api/company');
        $response->assertStatus(422)->assertJsonValidationErrors(['name'=>'The name field is required.']);
    }

    /**
     * @test
     */
    public function company_created()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->json('POST','/api/company',['name'=>$this->faker->company]);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function company_created_and_admin_assigned()
    {
        $user = User::factory()->create();
        $name = $this->faker->company;
        $response = $this->actingAs($user)->json('POST','/api/company',['name'=>$name]);
        $response->assertStatus(200);
        $company = Company::where('name',$name)->first();
        $companyAdmin = CompanyUser::where('company_id',$company->id)->where('user_id',$user->id)->first();
        $this->assertSame(1,$companyAdmin->is_admin);
    }

    /**
     * @test
     */
    public function company_profile_is_editable()
    {

        $user = User::factory()->create();
        $name = $this->faker->company;
        $response = $this->actingAs($user)->json('POST','/api/company',['name'=>$name]);
        $response->assertStatus(200);
        $company = Company::where('name',$name)->first();

        $updateData = [
            'legal_name'=>$this->faker->company,
            'legal_number'=>(string)$this->faker->randomNumber(5),
            'phone_number'=>$this->faker->e164PhoneNumber,
            'about'=>$this->faker->text(100),
            'email'=>$this->faker->safeEmail,
        ];
        $updateResponse = $this->actingAs($user)->json('PUT','/api/company/'.$company->id,$updateData);
        $updateResponse->assertJson(['data'=>['companies'=>['0'=>$updateData]]]);

    }

    /**
     * @test
     */
    function company_avatar_is_updatable()
    {
        $user = User::factory()->create();
        $name = $this->faker->company;
        $response = $this->actingAs($user)->json('POST','/api/company',['name'=>$name]);
        $response->assertStatus(200);
        $company = Company::where('name',$name)->first();

        $updateData = [
            'avatar'=>UploadedFile::fake()->create('test.png',0)
        ];
        $updateResponse = $this->actingAs($user)->post('/api/company/'.$company->id.'/avatar',$updateData);
        $updateResponse->assertStatus(200);
    }

    /**
     * @test
     */
    function company_cover_is_updatable()
    {
        $user = User::factory()->create();
        $name = $this->faker->company;
        $response = $this->actingAs($user)->json('POST','/api/company',['name'=>$name]);
        $response->assertStatus(200);
        $company = Company::where('name',$name)->first();

        $updateData = [
            'cover'=>UploadedFile::fake()->create('test.png',0)
        ];
        $updateResponse = $this->actingAs($user)->post('/api/company/'.$company->id.'/cover',$updateData);
        $updateResponse->assertStatus(200);
    }

    /**
     * @test
     */
    function get_users_company_list()
    {
        $user = User::factory()->create();
        $name = $this->faker->company;
        $name2 = $this->faker->company;
        $this->actingAs($user)->json('POST','/api/company',['name'=>$name])->assertStatus(200);
        $this->actingAs($user)->json('POST','/api/company',['name'=>$name2])->assertStatus(200);
        $list = $this->actingAs($user)->json('GET','/api/company');
        $list->assertStatus(200);
    }
}
