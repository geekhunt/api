<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateJobTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * @test
     */
    public function job_add_validation()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->create();
        $name = $this->faker->company;
        $this->actingAs($user)->json('POST','/api/company',['name'=>$name])->assertStatus(200);
        $company = Company::all()->first();
        $jobAddResponse = $this->actingAs($user)->json('POST','/api/company/'.$company->id.'/job',['name'=>$name]);

        $jobAddResponse->assertStatus(422);
    }
}
