<?php


namespace App\Helpers;


class CarbonGe
{
    const GE = [
        'minute' => ':count წუთის',
        'day' => ':count დღის',
        'hour' => ':count საათის',
        'week' => ':count კვირის',
        'ago' => ':time წინ',
        'months' => ['იანვარი', 'თებერვალი', 'მარტი', 'აპრილი', 'მაისი', 'ივნისი', 'ივლისი', 'აგვისტო', 'სექტემბერი', 'ოქტომბერი', 'ნოემბერი', 'დეკემბერი'],

    ];
}
