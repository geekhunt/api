<?php

namespace App\Mail;

use App\Models\User;
use App\Models\UserResume;
use App\Models\Vacancy;
use App\Models\VacancyCandidates;
use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class ResumeApply extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $candidate;
    protected $vacancy;
    protected $resume;
    protected $user;
    public function __construct(VacancyCandidates $candidate,Vacancy $vacancy,UserResume $resume,User $user)
    {
        $this->candidate = $candidate;
        $this->vacancy = $vacancy;
        $this->resume = $resume;
        $this->user = $user;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.resume-apply')->with([
            'candidate'=>$this->candidate,
            'vacancy'=>$this->vacancy,
            'resume'=>$this->resume,
            'user'=>$this->user
        ])->subject($this->vacancy->title);
    }
}
