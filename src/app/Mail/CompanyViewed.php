<?php

namespace App\Mail;

use App\Models\Company;
use App\Models\Vacancy;
use App\Models\VacancyCandidates;
use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class CompanyViewed extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $candidate;
    public $subject = 'რეზიუმე ჩამოტვირთულია';

    public function __construct(VacancyCandidates $candidate)
    {
        $this->candidate = $candidate;
    }

    public function build()
    {
        return $this->markdown('emails.company-viewed')
            ->with(
            [
                'greeting' => 'გამარჯობა, ' . $this->candidate->user->name,
                'introLines' => [
                    'თქვენი რეზიუმე ჩამოტვირთა კომპანია ' . $this->candidate->vacancy->company->name . ' - მა'
                ],
                'outroLines'=>[],
                'actionText' => 'ვაკანსიის ნახვა',
                'actionUrl' => '#',
                'level' => 'success',
                'color' => 'success'
            ]
        );
    }
}
