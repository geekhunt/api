<?php

namespace App\Observers;

use App\Models\Vacancy;

class VacancyObserver
{
    /**
     * Handle the Vacancy "created" event.
     *
     * @param  \App\Models\Vacancy  $vacancy
     * @return void
     */
    public function created(Vacancy $vacancy)
    {
        \Cache::forget('index');
    }

    /**
     * Handle the Vacancy "updated" event.
     *
     * @param  \App\Models\Vacancy  $vacancy
     * @return void
     */
    public function updated(Vacancy $vacancy)
    {
        \Cache::forget('index');
        \Cache::forget('vacancy-view-'.$vacancy->id);
    }

    /**
     * Handle the Vacancy "deleted" event.
     *
     * @param  \App\Models\Vacancy  $vacancy
     * @return void
     */
    public function deleted(Vacancy $vacancy)
    {
        \Cache::forget('index');
        \Cache::forget('vacancy-view-'.$vacancy->id);
    }

    /**
     * Handle the Vacancy "restored" event.
     *
     * @param  \App\Models\Vacancy  $vacancy
     * @return void
     */
    public function restored(Vacancy $vacancy)
    {
        \Cache::forget('index');
        \Cache::forget('vacancy-view-'.$vacancy->id);
    }

    /**
     * Handle the Vacancy "force deleted" event.
     *
     * @param  \App\Models\Vacancy  $vacancy
     * @return void
     */
    public function forceDeleted(Vacancy $vacancy)
    {
        \Cache::forget('index');
        \Cache::forget('vacancy-view-'.$vacancy->id);
    }
}
