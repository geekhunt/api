<?php

namespace App\Observers;

use App\Models\CompanyFollower;

class CompanyFollowerObserver
{
    /**
     * Handle the company follower "created" event.
     *
     * @param CompanyFollower $companyFollower
     */
    public function created(CompanyFollower $companyFollower)
    {
        \Cache::forget('company-view-'.$companyFollower->company_id);
    }

    /**
     * Handle the company follower "updated" event.
     *
     * @param CompanyFollower $companyFollower
     */
    public function updated(CompanyFollower $companyFollower)
    {
        \Cache::forget('company-view-'.$companyFollower->company_id);
    }

    /**
     * Handle the company follower "deleted" event.
     *
     * @param CompanyFollower $companyFollower
     */
    public function deleted(CompanyFollower $companyFollower)
    {
        \Cache::forget('company-view-'.$companyFollower->company_id);
    }

    /**
     * Handle the company follower "restored" event.
     *
     * @param CompanyFollower $companyFollower
     */
    public function restored(CompanyFollower $companyFollower)
    {
        \Cache::forget('company-view-'.$companyFollower->company_id);
    }

    /**
     * Handle the company follower "force deleted" event.
     *
     * @param CompanyFollower $companyFollower
     */
    public function forceDeleted(CompanyFollower $companyFollower)
    {
        \Cache::forget('company-view-'.$companyFollower->company_id);
    }
}
