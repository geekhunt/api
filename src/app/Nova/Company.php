<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Slug;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;
use Yna\NovaSwatches\Swatches;

class Company extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Company::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id','name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('სახელი','name')->sortable(),
            BelongsTo::make( 'ადმინი','admin','App\Nova\User')->nullable(),
            Slug::make('slug')->from('legal_name'),

            Text::make('ელ-ფოსტა','email'),
            Text::make('საიტი','website'),
            Markdown::make("კომპანიის შესახებ",'about')->hideFromIndex(),
            Number::make('დაფუძნებულია','founded')->hideFromIndex(),
            new Panel('იურიდიული ინფორმაცია',[
                Text::make('იურიდიული დასახელება','legal_name')->required(),
                Number::make('საიდენტიფიკაციო კოდი','legal_number'),
                Text::make('ტელეფონის ნომერი','phone_number'),
                Text::make('საქმიანობის სფერო','field_area'),
            ]),
            new Panel('ბრენდირება',[
                Swatches::make('ბრენდის ფერი','brand_color')->hideFromIndex(),
                Swatches::make('ბექგრაინდის ფერი','background_color')->hideFromIndex(),
                Avatar::make('ავატარი','avatar')->disk(config('filesystems.default')),
                Image::make('ქოვერი','cover')->disk(config('filesystems.default'))->hideFromIndex(),

            ]),
            new Panel('სტატუსი',[
                Boolean::make('აქტიური','is_active'),
                Boolean::make('არქივირებული','is_archived'),


            ]),
                HasMany::make('ვაკანსიები','activeJobs','App\Nova\Vacancies'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public static function label()
    {
        return 'კომპანიები';
    }


}
