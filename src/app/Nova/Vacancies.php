<?php

namespace App\Nova;

use App\Nova\Filters\ActiveVacancies;
use Illuminate\Http\Request;
use Laraning\NovaTimeField\TimeField;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Slug;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;
use Spatie\TagsField\Tags;

class Vacancies extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Vacancy::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        $defaultContractType = \App\Models\ContractType::where('is_default',1)->pluck('id');
        $defaultSalaryType = \App\Models\SalaryType::where('is_default',1)->pluck('id');
        $defaultCurrency = \App\Models\Currency::where('is_default',1)->pluck('id');
        return [
            ID::make(__('ID'), 'id')->sortable(),

            Text::make('დასახელება', 'title')->required()->size('w-1/3'),
            BelongsTo::make('კომპანია', 'company', 'App\Nova\Company')->searchable()->size('w-1/3')->required(),
            Slug::make('slug')->from('title')->hideFromIndex()->size('w-1/3'),
            Markdown::make('აღწერა', 'description')->required()->hideFromIndex()->size('w-full'),

            new Panel('სამუშაოს ტიპი',[
                BelongsTo::make('სპეციალიზაცია', 'specialisation', 'App\Nova\VacancySpecialisation')->required()->size('w-1/4'),
                BelongsTo::make('კონტრაქტის ტიპი', 'contract_type', 'App\Nova\ContractType')->withMeta([
                    'belongsToId' => $defaultContractType
                ])->nullable()->size('w-1/4')->hideFromIndex(),
                Boolean::make('ფრილანსი','is_freelance')->size('w-1/4')->hideFromIndex(),
                Boolean::make('დასაშვებია დისტანციური','is_remote')->size('w-1/4')->hideFromIndex(),
            ]),
            new Panel('სამუშაო საათები',[
               TimeField::make('დასყება','work_start_time')->hideFromIndex(),
               TimeField::make('დასყება','work_end_time')->hideFromIndex(),
            ]),
            new Panel('ანაზღაურება', [
                BelongsTo::make('ანაზღაურების ტიპი', 'salary_type', 'App\Nova\salaryType')->withMeta([
                    'belongsToId'=>$defaultSalaryType
                ])->nullable()->size('w-1/4')->hideFromIndex(),
                Number::make('ანაზღაურება - დან', 'salary_start')->size('w-1/4')->hideFromIndex(),
                Number::make('ანაზღაურება - მდე', 'salary_end')->size('w-1/4')->hideFromIndex(),
                BelongsTo::make('ვალუტა', 'currency', 'App\Nova\Currency')->withMeta([
                    'belongsToId'=>$defaultCurrency
                ])->nullable()->size('w-1/4')->hideFromIndex(),
            ]),
            new Panel('გამოქვეყნება',[
                Text::make('მეილი','email')->help('მეილი სადაც მივა რეზიუმეები, თუ დატოვებთ ცარიელს იგულისხმება კომპანიის მეილი'),
               Date::make('გამოჩენის დრო','start_date')->required()->default(now())->hideFromIndex(),
               Date::make('ბოლო ვადა','end_date')->required()->default(now()->addDays(30)),
                Boolean::make('გააქტიურება','is_active'),
                Boolean::make('არის თუ არა დრაფტი','is_draft')->hideFromIndex(),
            ]),
            new Panel('ტეგები',[
                Tags::make('საჭირო სქილები')->type('skills')->hideFromIndex(),
            ])
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [new ActiveVacancies()];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    public static function label()
    {
        return 'ვაკანსიები';
    }
}
