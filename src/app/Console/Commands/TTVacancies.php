<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\Specialisation;
use App\Models\Vacancy;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class TTVacancies extends Command
{
    protected $signature = 'import:tt_vacancies';

    protected $description = 'Command description';

    protected $url = 'https://api.tt.ge/search/jobs?take=100&withCount=true&state=Active';
    public function handle()
    {
        $response =  Http::get($this->url);
        foreach ($response['items'] as $vacancy) {
            $companyName = $vacancy['company']['name'];
            $companyModel = Company::where('slug',$vacancy['company']['slug'])->first();
            $startDate = Carbon::make($vacancy['createdAt']);
            $endDate = clone $startDate;
            $endDate->addDays(30);
            if($companyModel)
            {
                $data = [
                    'title'=>$vacancy['name'],
                    'slug'=>$vacancy['slug'],
                    'description'=>$vacancy['description'],
                    'company_id'=>$companyModel->id,
                    'specialisation_id'=>1,
                    'start_date'=>$startDate,
                    'end_date'=>$endDate,
                    'salary_type_id'=>1,
                    'salary_currency'=>981,
                    'is_freelance'=>0,
                    'is_remote'=>0,
                    'work_start_time'=>'09:00:00',
                    'work_end_time'=>'18:00:00',
                    'location'=>$vacancy['location'],
                    'is_active'=>1,
                    'is_draft'=>0,
                ];
                $vacancyNewItem = Vacancy::create($data);
                $vacancyNewItem->syncTagsWithType($vacancy['tags'],'skills');
                /**
                $table->string('title');
                $table->string('slug');
                $table->text('description');
                $table->foreignId('company_id')->constrained('companies')->references('id');
                $table->foreignId('specialisation_id')->constrained('specialisations')->references('id');
                $table->date('end_date')->nullable();
                $table->date('start_date')->nullable();
                $table->integer('views')->default(0);
                $table->tinyInteger('is_freelance')->default(0);
                $table->tinyInteger('is_remote')->default(0);
                $table->char('work_start_time',8);
                $table->char('work_end_time',8);
                $table->decimal('salary_start',8,2,true)->nullable();
                $table->decimal('salary_end',8,2,true)->nullable();
                $table->integer('salary_currency')->default(981)->nullable();
                $table->foreignId('salary_type_id')->constrained('salary_types')->references('id');
                $table->foreignId('contract_type_id')->default(1)->constrained('contract_types')->references('id');
                $table->boolean('is_draft')->default(true);
                $table->boolean('is_active')->default(false);
                 *
                 */
            }
        }
    }
}
