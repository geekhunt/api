<?php

namespace App\Console\Commands;

use App\Models\Company;
use Carbon\Carbon;
use Faker\Provider\UserAgent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use PHPHtmlParser\Dom;

class ParseJobsGe extends Command
{
    protected $signature = 'parser:jobsge';

    protected $description = 'Command description';
    protected $url = 'https://www.jobs.ge/?page=1&q=&cid=6&lid=&jid=';
    protected $months = ['იანვარი','თებერვალი','მარტი','აპრილი','მაისი','ივნისი','ივლისი','აგვისტო','სექტემბერი','ოქტომბერი','ნოემბერი','დეკემბერი'];
    public function handle()
    {
        $data = Http::withHeaders([
            'user-agent'=>UserAgent::userAgent(),
            'authority'=> 'www.jobs.ge'
        ])->get($this->url);
        $dom = new Dom;
        $dom->loadStr($data->body());
        $container = $dom->find('#job_list_table');
         $container->find('tbody');
        $tr =  $container->find('tr');
        $i = 0;
        $lastId = DB::table('jobs_source')->where('source','jobs.ge')->orderBy('source_id','desc')->first();
        if($lastId)
        {
            $last_id = $lastId->source_id;
        }
        else{
            $last_id = 0;
        }
        foreach ($tr as $item) {
            if($i==0)
            {
                $i++;
                continue;
            }
            $td = $item->find('td');
            $id = trim($td[0]->find('img')->getAttribute('id'));
            $title = trim(strip_tags($td[1]->innerHtml));
            $company = trim(strip_tags($td[3]->innerHtml));
            $start_date = trim(strip_tags($td[4]->innerHtml));
            $end_date = trim(strip_tags($td[5]->innerHtml));

            if($id>$last_id)
            {
            $internalCompany = Company::where('name',$company)->orWhere('legal_name',$company)->first();
            if($internalCompany)
            {
                $internal_id = $internalCompany->id;
            }
            else{
                $internal_id = null;
            }
            DB::table('jobs_source')->insert([
                'source'=>'jobs.ge',
                'source_id'=>$id,
                'company_id'=>$internal_id,
                'title'=>$title,
                'company'=>$company,
                'start_date'=>$this->getDate($start_date),
                'end_date'=>$this->getDate($end_date),
                'log_date'=>now(),
            ]);
            }
            $i++;
        }

    }

    public function getDate($string)
    {
        $splitted = explode(' ',$string);
        $monthIndex = array_search($splitted['1'],$this->months);
        return Carbon::createFromFormat('Y-m-d',date('Y').'-'.($monthIndex+1).'-'.$splitted['0']);
    }
}
