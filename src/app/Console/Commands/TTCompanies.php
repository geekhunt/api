<?php

namespace App\Console\Commands;

use App\Models\Company;
use Illuminate\Console\Command;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Http;

class TTCompanies extends Command
{
    protected $signature = 'import:tt_companies';

    protected $description = 'Command description';
    protected string $url = 'https://api.tt.ge/search/companies?state=Active&take=100&withCount=true';

    public function handle()
    {
       $response =  Http::get($this->url);
        foreach ($response['items'] as $company)
        {
            $data = [];
            $data['name'] = $company['name'];
            $data['about'] = $company['about'];
            $data['slogan'] = $company['primaryText'];
            $data['legal_name'] = $company['name'];
            $data['slug'] = $company['slug'];
            $data['founded'] = is_numeric($company['founded']) ? $company['founded'] : null;
            $data['brand_color'] = $company['primaryTextColor'];
            $data['background_color'] = $company['backgroundColor'];
            $data['website'] = $company['url'];
            $data['is_active'] = 1;
            $url = 'https://s3.eu-central-1.amazonaws.com/public.tt.ge'.$company['logoUrl'];
            $info = pathinfo($url);
            $contents = file_get_contents($url);
            $file = '/tmp/' . $info['basename'];
            file_put_contents($file, $contents);
            $file = new File($file);
            $storagePath = \Storage::disk(config('filesystems.default'))->put('companies',$file);
            $data['avatar'] = $storagePath;
            Company::updateOrCreate(['slug'=>$data['slug']],$data);
//            dd();
        }
    }
}
