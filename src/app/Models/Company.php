<?php

namespace App\Models;

use App\Traits\Archivable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Company extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function scopeActive($query)
    {
        return $query->where('is_archived',0);
    }
    public function getInitialsAttribute()
    {
        $explode = explode(' ',$this->name);
        if(count($explode)>1 && is_string($explode[0]) && is_string($explode[1]))
        {
            return mb_substr($explode['0'],0,1).''.mb_substr($explode['1'],0,1);
        }
        else{
            return mb_substr($this->name,0,2);
        }


    }
    public static function getCompanyList()
    {
        return self::withCount('activeJobs')->active()->withCount('followers')->simplePaginate();
    }

    public function activeJobs()
    {
        return $this->hasMany(Vacancy::class)->scopes(['active']);
    }
    public function jobs()
    {
        return $this->hasMany(Vacancy::class);
    }
    public function followers()
    {
        return $this->hasMany(CompanyFollower::class);
    }
    public function getFollowersCountAttribute()
    {
        return $this->hasMany(CompanyFollower::class)->count();
    }
    public function getActiveJobsCountAttribute()
    {
        return $this->hasMany(Vacancy::class)->scopes(['active'])->count();
    }
    public function admin()
    {
        return $this->BelongsTo(User::class);
    }
}
