<?php

namespace App\Models;

use App\Notifications\VerifyEmailQueued;
use App\Notifications\VerifyUserEmail;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'last_name',
        'birth_date',
        'gender',
        'phone',
        'title',
        'about',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
        'birth_date'=>'date'
    ];


    public function getInitialsAttribute()
    {
        if(!empty($this->name) && !empty($this->last_name))
        {
            return mb_substr($this->name,0,1).''.mb_substr($this->last_name,0,1);
        }
        else{
            return mb_substr($this->name,0,2);
        }
    }
    public function companies()
    {
        return $this->belongsToMany(Company::class,'company_users')->withPivot(['is_admin','is_hr','is_follower']);
    }

    public function followedCompanies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class,'company_followers');
    }

    public function savedVacancies(): BelongsToMany
    {
        return $this->belongsToMany(Vacancy::class,'saved_vacancies','user_id','vacancy_id');
    }
    public function appliedVacancies(): BelongsToMany
    {
        return $this->belongsToMany(Vacancy::class,'vacancy_candidates','user_id','vacancy_id');
    }

    public function resumes(): HasMany
    {
        return $this->hasMany(UserResume::class);
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailQueued());
    }
}
