<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobsSource extends Model
{
    protected $table = 'jobs_source';
    protected $casts = ['start_date'=>'date','end_date'=>'date','log_date'=>'datetime'];
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
