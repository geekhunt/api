<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacancyCandidates extends Model
{
    protected $guarded = [];


    public function resume()
    {
        return $this->hasOne(UserResume::class,'id','resume_id');
    }
    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    public function vacancy()
    {
        return $this->hasOne(Vacancy::class,'id','vacancy_id');
    }
}
