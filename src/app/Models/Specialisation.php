<?php

namespace App\Models;

use App\Traits\Approvable;
use App\Traits\CreateOrSelectKeyValue;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Specialisation extends Model
{
    use HasFactory;
    use CreateOrSelectKeyValue;
    use Approvable;

    protected $visible = ['id','name','slug'];
}
