<?php

namespace App\Models;

use App\Traits\Archivable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Tags\HasTags;

class Vacancy extends Model
{
    use Archivable;
   use HasTags;

    protected $table = 'vacancies';
    protected $guarded = [];
    protected $casts = [
        'start_date'=>'date',
        'end_date'=>'date'
    ];

    /**
     * მხოლოდ აქტიური ვაკანსიები
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1)
            ->where('is_draft', 0)
            ->where('end_date', '>=', now()->format('Y-m-d'))
            ->where('start_date', '<=', now()->format('Y-m-d'));
    }

    /**
     * აქტიური ვაკანსიების სია
     * @param bool $showAll
     * @return mixed
     */
    public static function activeJobs(bool $showAll = false)
    {
        if ($showAll) {
            return self::scopes('active')->orderBy('id','desc')->get();
        }
        return self::scopes('active')->orderBy('id','desc')->simplePaginate();
    }

    /**
     * Company
     * @return BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return BelongsTo
     */
    public function specialisation()
    {
        return $this->belongsTo(Specialisation::class);
    }

    /**
     * @return BelongsTo
     */
    public function salary_type()
    {
        return $this->belongsTo(SalaryType::class);
    }

    public function contract_type(): BelongsTo
    {
        return $this->belongsTo(ContractType::class);
    }

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, 'salary_currency','id');
    }

    /**
     * @return HasMany
     */
    public function candidates(): HasMany
    {
        return $this->hasMany(VacancyCandidates::class, 'vacancy_id','id');
    }
}
