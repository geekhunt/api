<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class CompanyFollower extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function scopeMy($query,$userId)
    {
        return $query->where('user_id',$userId);
    }
    public function companies(): HasOne
    {
        return $this->hasOne(Company::class,'id','company_id');
    }
}
