<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

class Avatar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar'=>'required|image|mimetypes:image/jpeg,image/png,image/jpg'

        ];
    }

    public function validated()
    {
        $data = parent::validated();
        if(key_exists('avatar',$data)) {
            $image = \Auth::user()->avatar;
                try{

                    Image::load($this->file('avatar'))->fit(Manipulations::FIT_CONTAIN,256,256)->save();

                    $storagePath = \Storage::disk(config('filesystems.default'))->put('user_files/'.\Auth::id(),$this->file('avatar'));
                    if ($storagePath) {
                        $data['file']=\Storage::disk(config('filesystems.default'))->url($storagePath);
                    }
                    if(!empty($image)) {
                        $path = parse_url($image);
                        \Storage::disk(config('filesystems.default'))->delete($path['path']);
                    }
                }catch (\Exception $exception)
                {
                    throw  ValidationException::withMessages(['avatar'=>$exception->getMessage()]);
                }
        }
        else{
            throw  ValidationException::withMessages(['avatar'=>'file not found']);
        }
        return $data;
    }
}
