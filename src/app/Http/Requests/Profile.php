<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Profile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'=>'sometimes|string|max:255',
            'last_name'=>'sometimes|string|max:255',
            'phone'=>'sometimes|numeric',
            'birth_date'=>'sometimes|date',
            'gender'=>'sometimes|in:male,female,none',
            'title'=>'sometimes|max:255',
            'about'=>'sometimes|max:1024',
        ];
    }

}
