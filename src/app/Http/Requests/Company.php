<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class Company extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'=>'required|string|max:255|unique:companies,name',
            'legal_name'=>'sometimes|string|max:255',
            'legal_number'=>'sometimes|numeric',
            'phone_number'=>'sometimes|numeric',
            'email'=>'sometimes|email',
            'about'=>'sometimes|string|max:1024',
        ];
        if($this->isMethod('PUT'))
        {
            unset($rules['name']);
        }
        return $rules;
    }

    public function validated(): array
    {
        $data = parent::validated();

        if($this->isMethod('POST')) {
            $data['slug'] = Str::slug($data['name']);
        }
        return $data;
    }

}
