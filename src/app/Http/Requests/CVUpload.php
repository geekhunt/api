<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class CVUpload extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'resume'=>'required|mimetypes:application/pdf,application/doc|max:10000',
            'name'=>'sometimes|string|max:255',
            'comment'=>'sometimes|string|max:255',
        ];
        if($this->isMethod('PUT'))
        {
            $rules['resume'] = 'sometimes|mimetypes:application/pdf,application/doc|max:10000';
        }
        return $rules;
    }

    public function validated()
    {
        $data =  parent::validated();
        $data['user_id'] = \Auth::id();
        if(count(\Auth::user()->resumes)===0)
        {
            $data['is_default'] = 1;
        }
        if(key_exists('resume',$data)) {

                try{

                    $storagePath = \Storage::disk(config('filesystems.default'))->put('user_files/'.\Auth::id(),$this->file('resume'));
                    if ($storagePath) {
                        $data['path']=\Storage::disk(config('filesystems.default'))->url($storagePath);
                        $data['hash'] = $this->file('resume')->hashName();
                    }
                }catch (\Exception $exception)
                {
                    throw new ValidationException('ფაილის ატვირთვა ვერ მოხერხდა');
                }


        }
        unset($data['resume']);
        return $data;
    }
}
