<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VacancySearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query'=>'sometimes|string|max:255|nullable',
            'specialisations'=>'sometimes|array|nullable',
            'specialisations.*'=>'sometimes|integer|nullable',
            'is_remote'=>'sometimes|boolean|nullable',
            'contract_types'=>'sometimes|array|nullable',
            'contract_types.*'=>'sometimes|integer|nullable',
            'location'=>'sometimes|string|nullable'
        ];
    }
}
