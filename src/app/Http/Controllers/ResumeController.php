<?php

namespace App\Http\Controllers;

use App\Mail\CompanyViewed;
use App\Models\VacancyCandidates;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ResumeController extends Controller
{
    public function download(VacancyCandidates $candidate,$hash)
    {
            return new CompanyViewed($candidate);
        if($hash!==$candidate->resume->hash)
        {
            return abort(404);
        }
        $candidate->seen_at = now();
        $candidate->viewed++;
//        if($candidate->status==='new')
//        {
            $candidate->status='seen';
            // send mail to user
            Mail::to($candidate->user->email)->queue(new CompanyViewed($candidate));
//        }
        $candidate->save();
        return redirect($candidate->resume->path);
    }
}
