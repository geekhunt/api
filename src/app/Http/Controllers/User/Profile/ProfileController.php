<?php

namespace App\Http\Controllers\User\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Avatar;
use App\Http\Requests\Profile as ProfileRequest;
use App\Http\Resources\Profile;
use App\Traits\ApiResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    use ApiResponder;

    /**
     * მომხმარებლის პირადი ინფორმაცია
     * @route /api/me
     * @return JsonResponse
     */
    public function getMyInfo(): JsonResponse
    {
        return $this->success(new Profile(Auth::user()));
    }
    /**
     * მომხმარებლის პირადი პროფილის განახლება
     * @route /api/me
     * @param ProfileRequest $request
     * @return JsonResponse
     */
    public function update(ProfileRequest $request): JsonResponse
    {
        Auth::user()->update($request->validated());
        return $this->success(['message' => 'Profile Updated']);
    }

    /**
     * @return JsonResponse
     */
    public function resendEmailVerificationLink(): JsonResponse
    {
        Auth::user()->sendEmailVerificationNotification();
        return $this->success(['message' => 'Link Sent','feedback'=>
            ['type'=>'success','message'=>'ელ-ფოსტის ვერიფიკაციის ლინკი გამოგზავნილია']]);
    }

    /**
     * @param Avatar $request
     * @return JsonResponse
     */
    public function upload(Avatar $request): JsonResponse
    {
         $data = $request->validated();
        \Auth::user()->avatar = $data['file'];
        \Auth::user()->save();
        \Auth::user()->refresh();
        return $this->success(['avatar'=>$data['file']]);
    }

    public function removeAvatar()
    {
        $avatar = \Auth::user()->avatar;
        if(!empty($avatar)) {
            $parsed = parse_url($avatar);
            \Storage::disk(config('filesystems.default'))->delete($parsed['path']);
        }
        \Auth::user()->avatar = null;
        \Auth::user()->save();
        \Auth::user()->refresh();
        return $this->success([]);
    }

    public function changePassword(Request $request){
        $request->validate([
            'password'=>'required|confirmed',
        ]);
        \Auth::user()->password = \Hash::make($request->get('password'));
        \Auth::user()->save();
        \Auth::user()->refresh();
        return $this->success([]);

    }
    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        auth()->user()->tokens()->delete();
        return $this->success(['message' => 'Tokens Revoked']);
    }
}
