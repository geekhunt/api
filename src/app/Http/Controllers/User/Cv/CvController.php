<?php

namespace App\Http\Controllers\User\Cv;

use App\Http\Controllers\Controller;
use App\Http\Requests\CVUpload;
use App\Http\Resources\UserCV;
use App\Http\Resources\UserCVCollection;
use App\Models\UserResume;
use App\Traits\ApiResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

class CvController extends Controller
{
    use ApiResponder;

    /**
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        return $this->success(new UserCVCollection(\Auth::user()->resumes));
    }
    public function show(UserResume $resume)
    {
        return $this->success(new UserCv($resume));
    }

    /**
     * @param CVUpload $cv
     * @return JsonResponse
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function store(CVUpload $cv): JsonResponse
    {
        $resume = UserResume::create($cv->validated());
        $resume->refresh();
        return $this->success(new UserCV($resume));
    }

    /**
     * @param UserResume $resume
     * @param CVUpload $cv
     * @return JsonResponse
     */
    public function update(UserResume $resume, CVUpload $cv): JsonResponse
    {
        $resume->update($cv->validated());
        $resume->refresh();
        return $this->success(new UserCV($resume));
    }

    public function destroy(UserResume $resume)
    {
        if($resume->user_id===\Auth::id()){

            $resume->delete();
            return $this->success([]);
        }
        abort(401);
    }

    public function makeDefault(UserResume $resume){
        if($resume->user_id===\Auth::id()){
            UserResume::where('user_id',\Auth::id())->update(['is_default'=>false]);
            $resume->is_default = true;
            $resume->save();
            return $this->success([]);
        }
        abort(401);
    }
}
