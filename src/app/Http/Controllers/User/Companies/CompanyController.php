<?php

namespace App\Http\Controllers\User\Companies;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\CompanyFollower;
use App\Traits\ApiResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psy\Util\Json;

class CompanyController extends Controller
{
    use ApiResponder;

    /**
     * კომპანიის ფოლოვერის დამატება
     * @route /api/companies/{company}/follow
     * @param Company $company
     * @param CompanyFollower $companyFollower
     * @return JsonResponse
     */
    public function follow(Company $company,CompanyFollower $companyFollower): JsonResponse
    {
        $companyFollower->updateOrCreate(['user_id'=>\Auth::id(),'company_id'=>$company->id],[]);
        return $this->success([]);
    }

    public function unFollow(Company $company, CompanyFollower $companyFollower): JsonResponse
    {
        $companyFollower->where(['user_id'=>\Auth::id(),'company_id'=>$company->id])->delete();
        return $this->success([]);
    }
}
