<?php

namespace App\Http\Controllers\User\Companies;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyShortCollection;
use App\Models\CompanyFollower;
use App\Models\User;
use App\Traits\ApiResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowedCompaniesController extends Controller
{
    use ApiResponder;

    /**
     * მომხმარებლის მიერ დაფოლოვებული კომპანიები
     * @route /api/me/followed/companies
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $companies = \Auth::user()->followedCompanies;
        return $this->success(new CompanyShortCollection($companies));
    }

}
