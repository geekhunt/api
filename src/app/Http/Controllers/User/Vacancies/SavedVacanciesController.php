<?php

namespace App\Http\Controllers\User\Vacancies;

use App\Http\Controllers\Controller;
use App\Http\Resources\VacancyShortCollection;
use App\Models\Vacancy;
use App\Models\SavedVacancies;
use App\Models\User;
use App\Traits\ApiResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SavedVacanciesController extends Controller
{
    use ApiResponder;
    public function list()
    {
        return $this->success(new VacancyShortCollection(\Auth::user()->savedVacancies));
    }

    /**
     * @param Vacancy $vacancy
     * @param SavedVacancies $savedVacancies
     * @return JsonResponse
     */
    public function save(Vacancy $vacancy, SavedVacancies $savedVacancies): JsonResponse
    {
        $savedVacancies->updateOrCreate(['user_id'=>\Auth::id(),'vacancy_id'=>$vacancy->id],[]);
        return $this->success([]);
    }

    /**
     * @param Vacancy $vacancy
     * @param SavedVacancies $savedVacancies
     * @return JsonResponse
     */
    public function remove(Vacancy $vacancy, SavedVacancies $savedVacancies): JsonResponse
    {
        $savedVacancies->where(['user_id'=>\Auth::id(),'vacancy_id'=>$vacancy->id])->delete();
        return $this->success([]);
    }
}
