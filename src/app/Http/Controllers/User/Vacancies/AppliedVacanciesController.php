<?php

namespace App\Http\Controllers\User\Vacancies;

use App\Http\Controllers\Controller;
use App\Http\Resources\VacancyShort;
use App\Traits\ApiResponder;

class AppliedVacanciesController extends Controller
{
    use ApiResponder;
    public function list()
    {
        return $this->success(VacancyShort::collection(\Auth::user()->appliedVacancies));
    }
}
