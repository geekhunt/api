<?php

namespace App\Http\Controllers\User\Vacancies;

use App\Http\Controllers\Controller;
use App\Mail\ResumeApply;
use App\Models\Vacancy;
use App\Models\UserResume;
use App\Models\VacancyCandidates;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class VacancyController extends Controller
{
    use ApiResponder;
    public function apply(Vacancy $vacancy, UserResume $resume)
    {
        $candidate = VacancyCandidates::updateOrCreate(['user_id'=>\Auth::id(),'vacancy_id'=>$vacancy->id,'resume_id'=>$resume->id],[]);
        $email = !empty($vacancy->email) ? $vacancy->email : $vacancy->company->email;
        if(!empty($email)) {
            \Mail::to($email)->queue(new ResumeApply($candidate, $vacancy, $resume, \Auth::user()));
        }
        return $this->success([]);
    }
}
