<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Login;
use App\Traits\ApiResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthorisationController extends Controller
{
    use ApiResponder;


    /**
     * @param Login $request
     * @return JsonResponse
     */
    public function login(Login $request): JsonResponse
    {
        if (!Auth::attempt($request->validated())) {
            return $this->errorFeedback(
                'Credentials not match',
                'warning',
                'არასწორი მეილი ან პაროლი',
                401
            );
        }
        return response()->json(['token' => auth()->user()->createToken('API Token')->plainTextToken]);
    }
}
