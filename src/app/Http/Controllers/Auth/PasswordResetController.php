<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordRequest;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class PasswordResetController extends Controller
{
    use ApiResponder;
    public function forgot() {
        $credentials = request()->validate(['email' => 'required|email']);
        Password::sendResetLink($credentials);
        return $this->successFeedback("","success","პაროლის განახლების ლინკი გამოგზავნილია მითითებულ ელ ფოსტაზე");
    }

    public function reset(ResetPasswordRequest $request) {
        $reset_password_status = Password::reset($request->validated(), function ($user, $password) {
            $user->password = \Hash::make($password);
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return $this->error("თოქენი არ ემთხვევა");
        }

        return $this->successFeedback('წარმატება',
            'success',
            'პაროლი წარმატებით შეიცვალა');
    }
}
