<?php


namespace App\Http\Controllers\Auth;


use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController
{
    use AuthenticatesUsers;
    protected $redirectTo = 'https://geekhunt.ge';

}
