<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Registration;
use App\Models\User;
use App\Traits\ApiResponder;
use Illuminate\Http\JsonResponse;

class RegistrationController extends Controller
{
    use ApiResponder;
    /**
     * @param Registration $request
     * @return JsonResponse
     */
    public function register(Registration $request): JsonResponse
    {
        $user = User::create($request->validated())->sendEmailVerificationNotification();
        return $this->success([]);
    }
}
