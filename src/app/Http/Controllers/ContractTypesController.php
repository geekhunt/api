<?php

namespace App\Http\Controllers;

use App\Http\Resources\ContractTypes;
use App\Models\ContractType;
use Illuminate\Http\Request;

class ContractTypesController extends Controller
{
    public function list()
    {
        return ContractTypes::collection(ContractType::all());
    }
}
