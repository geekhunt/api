<?php

namespace App\Http\Controllers;

use App\Http\Resources\ContractTypes;
use App\Http\Resources\VacancyShortCollection;
use App\Models\Company;
use App\Models\ContractType;
use App\Http\Resources\Specialisation as SpecialisationResource;
use App\Models\Specialisation;
use App\Models\Vacancy;
use App\Traits\ApiResponder;
use DebugBar\DebugBar;

class IndexController extends Controller
{
    use ApiResponder;
    public function index()
    {
        return VacancyShortCollection::make(Vacancy::activeJobs(false));
        return $this->success([
            'specialisations'=>SpecialisationResource::collection(Specialisation::all()),
            'contract_types'=>ContractTypes::collection(ContractType::all()),
            'total_vacancies'=>Vacancy::active()->count(),
            'total_companies'=>Company::count(),
            'latest_vacancies'=>VacancyShortCollection::make(Vacancy::activeJobs(false)),
        ]);
//        return \Cache::rememberForever('index',function(){
//
//        });

    }
}
