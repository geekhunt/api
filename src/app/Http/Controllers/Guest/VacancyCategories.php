<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Http\Resources\Specialisation;
use App\Models\Specialisation as SpecialisationModel;
use Illuminate\Http\Request;

class VacancyCategories extends Controller
{
    public function list()
    {
        return Specialisation::collection(SpecialisationModel::all());
    }
}
