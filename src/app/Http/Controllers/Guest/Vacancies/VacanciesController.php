<?php

namespace App\Http\Controllers\Guest\Vacancies;

use App\Http\Controllers\Controller;
use App\Http\Requests\VacancySearchRequest;
use App\Http\Resources\Company as CompanyResource;
use App\Http\Resources\VacancyShortCollection;
use App\Models\Company;
use App\Models\Vacancy;
use App\Models\SavedVacancies;
use App\Traits\ApiResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Spatie\Fractal\Fractal;

class VacanciesController extends Controller
{
    use ApiResponder;

    /**
     * ვაკანსიების სია
     * @return VacancyShortCollection
     */
    public function list(): VacancyShortCollection
    {
        $showAll = false;
        if (\request()->has('show') && \request()->get('show') === 'all') {
            $showAll = true;
        }
        return VacancyShortCollection::make(Vacancy::activeJobs($showAll));
    }

    /**
     * კონკრეტული ვაკანსიის დაბრუნება
     * @param string $vacancySlug
     * @return \App\Http\Resources\Vacancy
     */
    public function view(string $vacancySlug): \App\Http\Resources\Vacancy
    {
        $exploded = explode('-', $vacancySlug);
        if (count($exploded) > 0 && is_numeric($exploded['0'])) {
            $vacancyId = abs(intval($exploded['0']));
            $vacancy = \Cache::rememberForever('vacancy-view-' . $vacancyId, function () use ($vacancyId) {
                return Vacancy::findOrFail($vacancyId);
            });
            return \App\Http\Resources\Vacancy::make($vacancy);
        }
        return abort(404);

    }

    /**
     * ვაკანსიის ძებნა
     * @param VacancySearchRequest $request
     * @return VacancyShortCollection
     */
    public function search(VacancySearchRequest $request): VacancyShortCollection
    {
        $statement = Vacancy::active();
        if (!empty($request->get('query'))) {
            $statement->where('title', 'like', '%' . $request->get('query') . '%');
        }
        if (is_array($request->get('specialisations')) && !empty($request->get('specialisations'))) {
            $statement->whereIn('specialisation_id', $request->get('specialisations'));
        }
        if ($request->get('is_remote') !== 0) {
            $statement->where('is_remote', $request->get('is_remote'));
        }
        if ($request->get('contract_type_id') !== null) {
            $statement->whereIn('contract_type_id', $request->get('contract_type_id'));
        }
        if (!empty($request->get('location'))) {
            $statement->where('location', 'like', '%' . $request->get('location') . '%');
        }
        return VacancyShortCollection::make($statement->get());
    }


}
