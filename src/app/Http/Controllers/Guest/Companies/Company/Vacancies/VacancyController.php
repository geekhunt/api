<?php

namespace App\Http\Controllers\Guest\Companies\Company\Vacancies;

use App\Http\Controllers\Controller;
use App\Http\Resources\VacancyShortCollection;
use App\Models\Company;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    public function getCompanyVacancies(Company $company)
    {
        return $this->success(new VacancyShortCollection($company->activeJobs));
    }
}
