<?php

namespace App\Http\Controllers\Guest\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanySearchRequest;
use App\Http\Resources\CompanyShort;
use App\Http\Resources\CompanyShortCollection;
use App\Models\Company;
use App\Http\Resources\Company as CompanyResource;
use App\Traits\ApiResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyController extends Controller
{
    use ApiResponder;

    /**
     * კომპანიების სიის ნახვა არაავტორიზირებული მომხმარებლისთვის
     * @route /api/companies
     * @return CompanyShortCollection
     */
    public function list(): CompanyShortCollection
    {
        return CompanyShortCollection::make(Company::getCompanyList());
    }

    /**
     * კონკრეტული კომპანიის ნახვა არაავტორიზირებული მომხმარებლისთვის
     * @route /api/companies/{company}
     * @param string $companySlug
     * @return CompanyResource
     */
    public function view(string $companySlug): CompanyResource
    {
        $exploded = explode('-',$companySlug);
        if(count($exploded)>0 && is_numeric($exploded['0'])) {
            $companyId = abs(intval($exploded['0']));
            $company = \Cache::rememberForever('company-view-' . $companyId, function () use ($companyId) {
                return Company::findOrFail($companyId);
            });
            return CompanyResource::make($company);
        }
        return abort(404);
    }

    public function search(CompanySearchRequest $request)
    {
        return CompanyShort::collection(Company::active()
            ->where('name','like','%'.$request->get('query').'%')->get());
    }
}
