<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Candidate extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->user;
        return [
            'user_id'=>$user->id,
            'name'=>$user->name,
            'last_name'=>$user->last_name,
            'title'=>$user->title,
            'about'=>$user->about,
            'avatar'=>$user->avatar,
            'resume'=>$this->resume->getFirstMediaUrl('resume'),
            'seen_at'=>$this->seen_at,
            'status'=>$this->status,
            'comment'=>$this->comment

        ];
    }
}
