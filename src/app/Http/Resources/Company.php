<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Company extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'initials'=>strtoupper($this->initials),
            'name'=>$this->name,
            'slug'=>$this->id.'-'.$this->name,
            'slogan'=>$this->slogan,
            'website'=>$this->website,
            'legal_name'=>$this->legal_name,
            'legal_number'=>$this->legal_number,
            'founded'=>$this->founded,
            'phone_number'=>$this->phone_number,
            'avatar'=>!empty($this->avatar) ? \Storage::url($this->avatar) : null,
            'brand_color'=> $this->brand_color,
            'background_color'=> $this->background_color,
            'cover'=>!empty($this->cover) ? \Storage::url($this->cover) : null,
            'about'=>$this->about,
            'jobs'=> VacancyOnlyShort::collection($this->activeJobs),
            'about'=>$this->about,
            'location'=>$this->location,
            'isVip'=>false,
        ];


    }
    public function with($request)
    {
        return [
            'meta'=>[
                'followers_count'=>$this->followersCount,
            ]
        ];
    }
}
