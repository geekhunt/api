<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyShortCollection extends ResourceCollection
{
    public $collects  = 'App\Http\Resources\CompanyShort';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function with($request)
    {
        return [
            'meta'=>[
            'total'=>\App\Models\Company::active()->count(),
            ]
        ];
    }
}
