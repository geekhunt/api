<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed id
 * @property mixed email
 * @property mixed name
 * @property mixed last_name
 * @property mixed phone
 * @property mixed birth_date
 * @property mixed gender
 * @property mixed title
 * @property mixed about
 */
class Profile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'=>$this->id,
            'email'=>$this->email,
            'initials'=>strtoupper($this->initials),
            'name'=>$this->name,
            'last_name'=>$this->last_name,
            'phone'=>$this->phone,
            'birth_date'=>!empty($this->birth_date) ? $this->birth_date->format('Y-m-d') : null,
            'gender'=>$this->gender,
            'title'=>$this->title,
            'about'=>$this->about,
            'avatar'=> !empty($this->avatar) ? $this->avatar : null,

            'followed_companies'=>$this->followedCompanies->pluck('id'),
            'saved_vacancies'=>$this->savedVacancies->pluck('id'),
            'applied_vacancies'=>$this->appliedVacancies->pluck('id'),
        ];
    }
}
