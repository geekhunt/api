<?php

namespace App\Http\Resources;

use App\Helpers\CarbonGe;
use Carbon\Carbon;
use Carbon\Translator;
use Illuminate\Http\Resources\Json\JsonResource;

class VacancyShort extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $translator = \Carbon\Translator::get('ge');

        $translator->setTranslations(CarbonGe::GE);
        Carbon::setLocale('ge');
        return [
            'id' => $this->id,
            'title' => trim($this->title),
            'slug' => $this->id.'-'.$this->slug,
            'create_date'=>$this->start_date->locale('ge')->diffForHumans(),
            'finish_date'=>$this->end_date->format('d').' '. $this->end_date->locale('ge')->monthName,
            'has_salary'=> ($this->salary_start || $this->salary_end),
            'company' => new CompanyShort($this->company),
            'skills' => TagCollection::make($this->tagsWithType('skills')),
            'specialisation' => $this->specialisation,
            'salary_type'=>$this->salary_type,
            'contract_type'=>$this->contract_type,
            'is_remote'=>$this->is_remote,
            'location'=>$this->location,
        ];
    }
}
