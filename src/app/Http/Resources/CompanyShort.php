<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyShort extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'initials'=>strtoupper($this->initials),
            'name'=>$this->name,
            'slug'=>$this->id.'-'.$this->slug,
            'slogan'=>$this->slogan,
            'website'=>$this->website,
            'brand_color'=> $this->brand_color,
            'background_color'=>$this->background_color,
            'avatar'=>!empty($this->avatar) ? \Storage::url($this->avatar) : null,
            'cover'=>!empty($this->cover) ? \Storage::url($this->cover) : null,
            'active_jobs_count'=>$this->active_jobs_count,
            'followers_count'=>$this->followers_count,
            'location'=>$this->location,
            'isVip'=>false,
        ];
    }
}
