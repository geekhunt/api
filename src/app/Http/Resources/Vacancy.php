<?php

namespace App\Http\Resources;

use App\Helpers\CarbonGe;
use Carbon\Translator;
use Illuminate\Http\Resources\Json\JsonResource;

class Vacancy extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $translator = Translator::get('ge');
        $translator->setTranslations(CarbonGe::GE);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->id.'-'.$this->slug,
            'description'=>$this->description,
            'start_date'=>$this->start_date->locale('ge')->diffForHumans(),
            'finish_date'=>$this->end_date->format('d').' '. $this->end_date->locale('ge')->monthName,
            'company' => new CompanyShort($this->company),
            'skills' => TagCollection::make($this->tagsWithType('skills')),
            'specialisation' => $this->specialisation,
            'salary_type'=>$this->salary_type,
            'has_salary'=> ($this->salary_start || $this->salary_end),
            'salary_start'=>$this->salary_start,
            'salary_end'=>$this->salary_end,
            'salary_currency'=>$this->currency->currency_sign,
            'contract_type'=>$this->contract_type,
            'work_start_time'=>$this->work_start_time,
            'work_end_time'=>$this->work_end_time,
            'is_freelance'=>$this->is_freelance,
            'is_remote'=>$this->is_remote,
            'location'=>$this->location,
        ];
    }
    public function with($request)
    {
        return [
            'meta'=>[
                'similar_jobs'=>VacancyShortCollection::make(\App\Models\Vacancy::active()->take(3)->inRandomOrder()->get())
            ]
        ];
    }
}
