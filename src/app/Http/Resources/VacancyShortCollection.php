<?php

namespace App\Http\Resources;

use App\Models\Company;
use App\Models\ContractType;
use App\Models\Specialisation;
use App\Models\Vacancy;
use Illuminate\Http\Resources\Json\ResourceCollection;

class VacancyShortCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\VacancyShort';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status'=>'Success',
            'message'=>null,
            'data' => $this->collection,

        ];
    }

    public function with($request)
    {
        return [
            'meta'=>[
                'specialisations'=>\App\Http\Resources\Specialisation::collection(Specialisation::all()),
                'contract_types'=>ContractTypes::collection(ContractType::all()),
                'total_vacancies'=>Vacancy::active()->count(),
                'total_companies'=>Company::count(),
            ]
        ];
    }
}
