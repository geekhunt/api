<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class NovaStaticsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->getMethod() === 'GET') {

            $routes = [
                '/nova-api/scripts/',
                '/nova-api/styles/',
            ];

            if (Str::of($request->getRequestUri())->startsWith($routes)) {
                return app(SetCacheControl::class)
                    ->handle($request, static function ($request) use ($next) {
                        return $next($request);
                    }, 'private;max_age=3600;etag');
            }
        }

        return $next($request);
    }

}
