<?php


namespace App\Traits;


trait Approvable
{
    public function scopeIsApproved($query){
        return $query->where('is_approved',1);
    }
}
