<?php


namespace App\Traits;


use Illuminate\Http\JsonResponse;

trait ApiResponder
{
    /**
     * Return a success JSON response.
     *
     * @param array|string $data
     * @param string|null $message
     * @param int|null $code
     * @return JsonResponse
     */
    protected function success($data, string $message = null, int $code = 200): JsonResponse
    {
        return response()->json([
            'status' => 'Success',
            'message' => $message,
            'data' => $data
        ], $code);
    }

    /**
     * Return an error JSON response.
     *
     * @param string|null $message
     * @param int $code
     * @param array|string|null $data
     * @return JsonResponse
     */
    protected function error(string $message = null, int $code = 500, $data = null): JsonResponse
    {
        return response()->json([
            'status' => 'Error',
            'message' => $message,
            'data' => $data
        ], $code);
    }

    /**
     * Return feedback to frontend
     * @param string|null $message
     * @param string|null $feedbackType
     * @param string|null $feedback
     * @param int $code
     * @param null $data
     * @return JsonResponse
     */
    protected function errorFeedback(string $message = null, string $feedbackType = null, string $feedback = null, int $code = 500, $data = null): JsonResponse
    {
        return response()->json([
            'status' => 'Error',
            'message' => $message,
            'feedback'=>['type'=>$feedbackType,'message'=>$feedback],
            'data' => $data
        ], $code);
    }

    /**
     * Return feedback to frontend
     * @param string|null $message
     * @param string|null $feedbackType
     * @param string|null $feedback
     * @param int $code
     * @param null $data
     * @return JsonResponse
     */
    protected function successFeedback(string $message = null, string $feedbackType = null, string $feedback = null, int $code = 200, $data = null): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'feedback'=>['type'=>$feedbackType,'message'=>$feedback],
            'data' => $data
        ], $code);
    }
}
