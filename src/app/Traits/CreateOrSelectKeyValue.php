<?php


namespace App\Traits;


use Illuminate\Support\Str;

trait CreateOrSelectKeyValue
{
    public static function createOrSelect(array $create,array $select)
    {
        $skill = self::where($select)->first();
        if(!$skill)
        {
            $skill = self::create(array_merge($create,$select));
        }
        return $skill;
    }
}
