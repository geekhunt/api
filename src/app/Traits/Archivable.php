<?php


namespace App\Traits;


trait Archivable
{
    /**
     * @return bool
     */
    public function archive(): bool
    {
        if($this->id>0)
        {
            $this->is_archived = 1;
            $this->archived_at = now();
            $this->save();
            return true;
        }
        return false;
    }    /**
 * @return bool
 */
    public function unArchive(): bool
    {
        if($this->id>0)
        {
            $this->is_archived = 0;
            $this->archived_at = null;
            $this->save();
            return true;
        }
        return false;
    }
}
