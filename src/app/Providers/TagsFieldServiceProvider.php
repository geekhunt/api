<?php


namespace App\Providers;



use Laravel\Nova\Http\Middleware\Authorize;

class TagsFieldServiceProvider extends \Spatie\TagsField\TagsFieldServiceProvider
{
    protected function routes()
    {
        if ($this->app->routesAreCached()) {
            return;
        }

        \Route::middleware(['nova', Authorize::class])
            ->prefix('nova-vendor/spatie/nova-tags-field')
            ->group(base_path().'/vendor/spatie/nova-tags-field/routes/api.php');
    }
}
