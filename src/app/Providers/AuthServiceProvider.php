<?php

namespace App\Providers;

use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\JobList;
use App\Models\VacancyCandidates;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\URL;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('user-is-company-member',function (Authenticatable $user,Company $company){
            return CompanyUser::where('user_id',$user->id)->where('company_id',$company->id)->exists();
        });
        Gate::define('user-is-company-admin',function (Authenticatable $user,Company $company){
            return CompanyUser::where('user_id',$user->id)->where('company_id',$company->id)->where('is_admin',1)->exists();
        });
        Gate::define('vacancy-is-company-child',function (Authenticatable $user,Company $company,JobList $job){
            return $company->id === $job->company_id;
        });
        Gate::define('candidate-is-applied-on-vacancy',function (Authenticatable $user,JobList $job,VacancyCandidates $candidate){
            return $candidate->vacancy_id === $job->id;
        });

    }
}
