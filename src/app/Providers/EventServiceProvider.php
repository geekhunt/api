<?php

namespace App\Providers;

use App\Models\Company;
use App\Models\CompanyFollower;
use App\Models\Vacancy;
use App\Observers\CompanyFollowerObserver;
use App\Observers\CompanyObserver;
use App\Observers\VacancyObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Vacancy::observe(VacancyObserver::class);
        Company::observe(CompanyObserver::class);
        CompanyFollower::observe(CompanyFollowerObserver::class);
    }
}
