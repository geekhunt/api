<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;

class VerifyEmailQueued extends VerifyEmail implements ShouldQueue
{
    use Queueable;

//    /**
//     * Get the verification URL for the given notifiable.
//     *
//     * @param  mixed  $notifiable
//     * @return string
//     */
//    protected function verificationUrl($notifiable)
//    {
//        $prefix = config('frontend.url') . config('frontend.email_verify_url');
//        $temporarySignedURL = URL::temporarySignedRoute(
//            'verification.verify', Carbon::now()->addMinutes(60), [
//                'id' => $notifiable->getKey(),
//                'hash' => sha1($notifiable->getEmailForVerification()),
//            ]
//        );
//
//        // I use urlencode to pass a link to my frontend.
//        return $prefix . urlencode($temporarySignedURL);
//    }

    protected function buildMailMessage($url)
    {
        $message = new MailMessage();
        $message
            ->subject(Lang::get('დაადასტურეთ ელ-ფოსტა'))
            ->greeting('გამარჯობათ!')
            ->line(Lang::get('გთხოვთ დააჭიროთ დადასტურების ღილაკს ელ-ფოსტის ვეიფიკაციისთვის'))
            ->action(Lang::get('ელ-ფოსტის ვერიფიკაცია'), $url)
            ->salutation('საუკეთესო სურვილებით!');

        return $message;

    }
}
