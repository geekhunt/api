<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Mailgun\Mailgun;

class VerifyUserEmail extends VerifyEmail
{
    use Queueable;
    protected $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user = '')
    {
        $this->user =  $user ?: \Auth::user();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        Log::info(config('mail.mailers.mailgun.client'));
        Log::info(config('mail.mailers.mailgun.domain'));

        $mgClient = Mailgun::create(config('mail.mailers.mailgun.client')); // For US servers
        $domain = config('mail.mailers.mailgun.domain');
        $actionUrl  = $this->verificationUrl($notifiable);
        Log::info($actionUrl);
        # Make the call to the client.
//        $result = $mgClient->messages()->send($domain, array(
//            'from'	=> 'GeekHunt <verify@geekhunt.ge>',
//            'to'	=> $this->user->email,
//            'subject' => 'Mail Verification',
//            'text'	=> 'Verifycation URL '.$actionUrl,
//        ));
//        Log::info($result);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    protected function buildMailMessage($url)
    {
        return (new MailMessage)
            ->subject(Lang::get('Verify Email Address'))
            ->line(Lang::get('Please click the button below to verify your email address.'))
            ->action('ელ-ფოსტის ვერიფიკაცია', $url)
            ->line(Lang::get('If you did not create an account, no further action is required.'));
    }
}
